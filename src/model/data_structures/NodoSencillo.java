package model.data_structures;

public class NodoSencillo<T> {
	
	private T item;
	private NodoSencillo<T> siguiente;
	
	public NodoSencillo( T pItem)
	{
		siguiente = null;
		item = pItem;
	}
	
	public NodoSencillo<T> darSiguiente()
	{
		return siguiente;
	}

	public void setSiguiente(NodoSencillo<T> psiguiente)
	{
	siguiente = psiguiente;
	}
	
	public T getItem()
	{
		return item;
	}
	
	public void setItem (T pItem)
	{
		item = pItem;
	}
}
