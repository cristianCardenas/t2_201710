package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Scanner;

import model.data_structures.ILista;
import model.data_structures.NoExisteElemento;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;

	BufferedReader reader = null;

	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) throws IOException {

		File file = new File(archivoPeliculas);

		try {
			String linea = "";
			reader = new BufferedReader(new FileReader(file));
			while((linea = reader.readLine()) != null && linea != "")
			{
	
			/*
				String [] pelicula= linea.split(",");
				int id= Integer.parseInt(pelicula[0]);
				String nombre= pelicula[1];
				String[] genero= pelicula[2].split("|");
				System.out.println("ID="+id+"Nombre:"+nombre+"Generos:"+genero);
				//VOPelicula peli= new VOPelicula().setTitulo(nombre);
				//peli.setAgnoPublicacion(id);
				//peli.setGenerosAsociados(genero);
				 
				 */
				String[] infoPel=linea.split("\"");
				if(infoPel.length==1)
				{
					String[] infoPelComas = linea.split(",");
					String titulo= infoPelComas[1];
					String genero[]= infoPelComas[2].split("|");
					String[] a�o= infoPelComas[1].split("(");
					titulo=a�o[0];
					String anio=a�o[1].substring(0, 2);
					int a�oP=Integer.parseInt(anio);
					misPeliculas.agregarElementoFinal(new VOPelicula(titulo,a�oP,genero));
					
				}
				else
				{
					//String id= infoPel[0];
					String var1= infoPel[1];
					String [] var2= var1.split("(");
					String nombre=var2[0];
					String anio= var2[1].substring(0, 2);
					int a�o =Integer.parseInt(anio);
					String[] gen= infoPel[2].split("|");
					misPeliculas.agregarElementoFinal(new VOPelicula(nombre,a�o,gen));
				}
				
				
			
			}
			
		} catch (Exception e) {
			//throw new Exception();
		
			// TODO: handle exception
		}
		finally
		{
			reader.close();
		}
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda)  {
		ILista<VOPelicula> peliculasEncontradas= new ILista<VOPelicula>() {

			@Override
			public Iterator<VOPelicula> iterator() {
				
				return misPeliculas.iterator();
			}

			@Override
			public void agregarElementoFinal(VOPelicula elem) {
				misPeliculas.agregarElementoFinal(elem);
				// TODO Auto-generated method stub
				
				
				
			}

			@Override
			public VOPelicula darElemento(int pos) throws NoExisteElemento {
				
					return	misPeliculas.darElemento(pos);
					
				
				// TODO Auto-generated method stub
				//return null;
			}

			@Override
			public int darNumeroElementos() {
			
				return misPeliculas.darNumeroElementos();
			}

			@Override
			public VOPelicula darElementoPosicionActual() {
			return	misPeliculas.darElementoPosicionActual();
			}

			@Override
			public boolean avanzarSiguientePosicion() {
			return misPeliculas.avanzarSiguientePosicion();
			}

			@Override
			public boolean retrocederPosicionAnterior() {
			return misPeliculas.retrocederPosicionAnterior();
			}
		};
		while(misPeliculas.avanzarSiguientePosicion()==true)
		{
   VOPelicula pel = misPeliculas.darElementoPosicionActual();
		if(pel.getTitulo().contains(busqueda)==true)
		{
			peliculasEncontradas.agregarElementoFinal(pel);
		}
		}
		return peliculasEncontradas;
		
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {


		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		return null;
	}

}
