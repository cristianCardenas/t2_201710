package controller;

import model.data_structures.ILista;
import model.logic.ManejadorPeliculas;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.IOException;

import api.IManejadorPeliculas;

public class Controller {

	
	private static IManejadorPeliculas manejador= new ManejadorPeliculas();

	public static void cargarPeliculas() throws IOException {
		
		manejador.cargarArchivoPeliculas("data/movies.csv");
		
	}

	public static ILista<VOPelicula> darListaPeliculas(String busqueda) {
		return manejador.darListaPeliculas(busqueda);
	}

	public static ILista<VOPelicula> darPeliculasPorAgno(int agno) {
		return manejador.darPeliculasAgno(agno);
	}
	
	public static VOAgnoPelicula darPeliculasAgnoSiguiente() {
		return manejador.darPeliculasAgnoSiguiente();
	}
	
	public static VOAgnoPelicula darPeliculasAgnoAnterior() {
		return manejador.darPeliculasAgnoAnterior();
	}

}
