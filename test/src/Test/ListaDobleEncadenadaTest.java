package Test;

import model.data_structures.ListaEncadenada;
import junit.framework.TestCase;


public class ListaDobleEncadenadaTest extends TestCase
{
	private ListaEncadenada<String> listaEncadenada;

	private void setUpEscenario1( )
	{
		listaEncadenada = new ListaEncadenada<String>();
	}

	private void setUpEscenario2( )
	{
		listaEncadenada = new ListaEncadenada<String>();

		/*0*/ listaEncadenada.agregarElementoFinal("Juan Pablo");
		/*1*/ listaEncadenada.agregarElementoFinal("Sebasti�n");
		/*2*/ listaEncadenada.agregarElementoFinal("Pedro");
		/*3*/ listaEncadenada.agregarElementoFinal("Camila");
		/*4*/ listaEncadenada.agregarElementoFinal("Sara");
	}

	public void testAgregarElementoFinal()
	{
		setUpEscenario1( );
		listaEncadenada.agregarElementoFinal("Mar�a");
		assertEquals("El elemento deberia ser Mar�a", "Mar�a", listaEncadenada.darElemento(0));
		
		setUpEscenario2( );
		listaEncadenada.agregarElementoFinal("Andrea");
		assertEquals("El elemento deberia ser Andrea", "Andrea", listaEncadenada.darElemento(5));
	}
	
	public void testDarElemento()
	{
		setUpEscenario2();
		assertEquals("El elemento deberia ser Sara", "Sara", listaEncadenada.darElemento(4));
	}

	public void testDarNumeroDeElementos()
	{
		setUpEscenario1( );
		listaEncadenada.agregarElementoFinal("Estructura");
		listaEncadenada.darNumeroElementos();
		assertEquals(1 , listaEncadenada.darNumeroElementos() );

		setUpEscenario2( );
		listaEncadenada.darNumeroElementos();
		assertEquals(5 , listaEncadenada.darNumeroElementos());
	}
	
	public void testAvanzarSiguientePosicion()
	{
		setUpEscenario1();
		listaEncadenada.avanzarSiguientePosicion();
		assertEquals("El nodo no deberia existir", false , listaEncadenada.avanzarSiguientePosicion());
		
		setUpEscenario2();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		
		assertEquals("El nodo no deberia existir", false , listaEncadenada.avanzarSiguientePosicion());
		
		setUpEscenario2();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		
		assertEquals("El nodo deberia existir", true , listaEncadenada.avanzarSiguientePosicion());
	}
	
	public void testRetrocederPosicionAnterior()
	{
		setUpEscenario1();
		listaEncadenada.retrocederPosicionAnterior();
		assertEquals("No se debe ejecutar porque hay un solo nodo", false , listaEncadenada.retrocederPosicionAnterior());
		
		setUpEscenario2();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.avanzarSiguientePosicion();
		listaEncadenada.retrocederPosicionAnterior();
		
		assertEquals("Debe retroceder a la posici�n anterior", true , listaEncadenada.retrocederPosicionAnterior());
	}

}
